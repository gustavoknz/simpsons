package com.kieling.simpsons.utils;

import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

import rx.Observable;

public final class Utils {
    private static final String TAG = "Utils";

    private static boolean isNetworkAvailable() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (Exception e) {
            Log.e(TAG, "Error checking connectivity", e);
        }
        return false;
    }

    public static Observable<Boolean> isNetworkAvailableObservable() {
        return Observable.just(isNetworkAvailable());
    }

    public static void showSnackBar(View view, String message, int length) {
        Snackbar.make(view, message, length).setAction("Action", null).show();
    }
}
