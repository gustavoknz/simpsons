package com.kieling.simpsons.api;

import com.kieling.simpsons.models.Simpchar;

import java.util.ArrayList;

import retrofit2.http.GET;
import rx.Observable;

public interface SimpcharApi {

    //j4g2c85pmf.execute-api.us-east-1.amazonaws.com/Prod/
    @GET("simpsons")
    Observable<ArrayList<Simpchar>> getSimpcharList();
}
