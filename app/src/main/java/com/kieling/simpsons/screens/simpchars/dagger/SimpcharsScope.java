package com.kieling.simpsons.screens.simpchars.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

@Scope
@Retention(RetentionPolicy.CLASS)
@interface SimpcharsScope {
}
