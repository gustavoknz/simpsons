package com.kieling.simpsons.screens.simpchars.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kieling.simpsons.R;
import com.kieling.simpsons.models.Simpchar;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.subjects.PublishSubject;

public class SimpcharsAdapter extends RecyclerView.Adapter<SimpcharViewHolder> {
    private final PublishSubject<Integer> itemClicks = PublishSubject.create();
    private final List<Simpchar> listSimpchars = new ArrayList<>();

    public void swapAdapter(List<Simpchar> simpcharList) {
        this.listSimpchars.clear();
        this.listSimpchars.addAll(simpcharList);
        notifyDataSetChanged();
    }

    public Observable<Integer> observeClicks() {
        return itemClicks;
    }

    @Override
    public SimpcharViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_simpchar, parent, false);
        return new SimpcharViewHolder(view, itemClicks);
    }

    @Override
    public void onBindViewHolder(SimpcharViewHolder holder, int position) {
        Simpchar simpchar = listSimpchars.get(position);
        holder.bind(simpchar);
    }

    @Override
    public int getItemCount() {
        return listSimpchars.size();
    }
}
