package com.kieling.simpsons.screens.simpchars.dagger;


import com.kieling.simpsons.application.builder.AppComponent;
import com.kieling.simpsons.screens.simpchars.SimpcharListActivity;

import dagger.Component;

@SimpcharsScope
@Component(dependencies = {AppComponent.class}, modules = {SimpcharsModule.class})
public interface SimpcharsComponent {
    void inject(SimpcharListActivity simpcharsActivity);
}
