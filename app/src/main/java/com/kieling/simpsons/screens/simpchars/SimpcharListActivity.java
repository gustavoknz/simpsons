package com.kieling.simpsons.screens.simpchars;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.kieling.simpsons.application.AppController;
import com.kieling.simpsons.screens.details.DetailsActivity;
import com.kieling.simpsons.screens.simpchars.core.SimpcharsPresenter;
import com.kieling.simpsons.screens.simpchars.core.SimpcharsView;
import com.kieling.simpsons.screens.simpchars.dagger.DaggerSimpcharsComponent;
import com.kieling.simpsons.screens.simpchars.dagger.SimpcharsModule;
import com.kieling.simpsons.utils.Constants;

import javax.inject.Inject;

public class SimpcharListActivity extends AppCompatActivity {

    @Inject
    SimpcharsView view;
    @Inject
    SimpcharsPresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerSimpcharsComponent.builder()
                .appComponent(AppController.getNetComponent())
                .simpcharsModule(new SimpcharsModule(this))
                .build()
                .inject(this);
        setContentView(view.view());
        presenter.onCreate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    public void goToDetailsActivity(String simpcharId) {
        Intent in = new Intent(this, DetailsActivity.class);
        in.putExtra(Constants.PARAM_SIMPCHAR_ID, simpcharId);
        startActivity(in);
    }
}
