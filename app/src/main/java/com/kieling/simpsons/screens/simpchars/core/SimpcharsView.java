package com.kieling.simpsons.screens.simpchars.core;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.kieling.simpsons.R;
import com.kieling.simpsons.models.Simpchar;
import com.kieling.simpsons.screens.simpchars.SimpcharListActivity;
import com.kieling.simpsons.screens.simpchars.list.SimpcharsAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;

public class SimpcharsView {

    private final View view;
    private final SimpcharsAdapter adapter;

    @BindView(R.id.activity_simpchars_list_recycler_view)
    RecyclerView list;

    public SimpcharsView(SimpcharListActivity context) {
        FrameLayout parent = new FrameLayout(context);
        parent.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        view = LayoutInflater.from(context).inflate(R.layout.activity_simpchar_list, parent, true);
        ButterKnife.bind(this, view);

        adapter = new SimpcharsAdapter();

        list.setAdapter(adapter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        list.setLayoutManager(mLayoutManager);
    }

    Observable<Integer> itemClicks() {
        return adapter.observeClicks();
    }

    public View view() {
        return view;
    }

    void setAdapter(ArrayList<Simpchar> simpchars) {
        adapter.swapAdapter(simpchars);
    }
}
