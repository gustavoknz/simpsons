package com.kieling.simpsons.screens.simpchars.core;

import com.kieling.simpsons.api.SimpcharApi;
import com.kieling.simpsons.models.Simpchar;
import com.kieling.simpsons.screens.simpchars.SimpcharListActivity;
import com.kieling.simpsons.utils.Utils;

import java.util.ArrayList;

import rx.Observable;

public class SimpcharsModel {
    private final SimpcharListActivity context;
    private final SimpcharApi api;

    public SimpcharsModel(SimpcharListActivity context, SimpcharApi api) {
        this.api = api;
        this.context = context;
    }

    Observable<ArrayList<Simpchar>> provideSimpcharList() {
        return api.getSimpcharList();
    }

    Observable<Boolean> isNetworkAvailable() {
        return Utils.isNetworkAvailableObservable();
    }

    void goToDetailsActivity(String simpcharId) {
        context.goToDetailsActivity(simpcharId);
    }
}
