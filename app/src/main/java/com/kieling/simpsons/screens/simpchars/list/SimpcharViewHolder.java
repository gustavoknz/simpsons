package com.kieling.simpsons.screens.simpchars.list;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kieling.simpsons.R;
import com.kieling.simpsons.models.Simpchar;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.subjects.PublishSubject;

class SimpcharViewHolder extends RecyclerView.ViewHolder {
    private final View view;

    @BindView(R.id.itemSimpcharPicture)
    ImageView imageSimpchar;
    @BindView(R.id.itemSimpcharFirstName)
    TextView simpcharFirstName;
    @BindView(R.id.itemSimpcharLastName)
    TextView simpcharLastName;

    SimpcharViewHolder(View itemView, PublishSubject<Integer> clickSubject) {
        super(itemView);
        this.view = itemView;
        ButterKnife.bind(this, view);
        view.setOnClickListener(v -> clickSubject.onNext(getAdapterPosition()));
    }

    void bind(Simpchar simpChar) {
        Glide.with(view.getContext()).load(simpChar.getPicture()).into(imageSimpchar);

        simpcharFirstName.setText(TextUtils.isEmpty(simpChar.getFirstName()) ? "missing first name" : simpChar.getFirstName());
        simpcharLastName.setText(TextUtils.isEmpty(simpChar.getLastName()) ? "missing last name" : simpChar.getLastName());
    }
}
