package com.kieling.simpsons.screens.simpchars.core;

import android.util.Log;

import com.kieling.simpsons.models.Simpchar;
import com.kieling.simpsons.utils.rx.RxSchedulers;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class SimpcharsPresenter {
    private static final String TAG = "SimpcharsPresenter";
    private final SimpcharsView view;
    private final SimpcharsModel model;
    private final RxSchedulers rxSchedulers;
    private final CompositeSubscription subscriptions;
    private List<Simpchar> simpchars = new ArrayList<>();

    public SimpcharsPresenter(RxSchedulers schedulers, SimpcharsModel model, SimpcharsView view, CompositeSubscription sub) {
        this.rxSchedulers = schedulers;
        this.view = view;
        this.model = model;
        this.subscriptions = sub;
    }

    public void onCreate() {
        Log.d(TAG, "Presenter init");
        subscriptions.add(getSimpcharList());
        subscriptions.add(respondToClick());
    }

    public void onDestroy() {
        subscriptions.clear();
    }

    private Subscription respondToClick() {
        return view.itemClicks().subscribe(item -> model.goToDetailsActivity(simpchars.get(item).get_id()));
    }

    private Subscription getSimpcharList() {
        return model.isNetworkAvailable()
                .doOnNext(networkAvailable -> {
                    if (!networkAvailable) {
                        Log.d(TAG, "no connection");
                        //TODO Utils.showSnackBar();
                    }
                })
                .filter(isNetworkAvailable -> true)
                .flatMap(isAvailable -> model.provideSimpcharList())
                .subscribeOn(rxSchedulers.internet())
                .observeOn(rxSchedulers.androidThread())
                .subscribe(simpchars -> {
                            Log.d(TAG, "info loaded");
                            view.setAdapter(simpchars);
                            this.simpchars = simpchars;
                        }, throwable -> {
                            Log.e(TAG, "Error getting Simpchar list", throwable);
                        }
                );
    }
}
