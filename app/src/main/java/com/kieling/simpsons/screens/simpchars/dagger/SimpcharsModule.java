package com.kieling.simpsons.screens.simpchars.dagger;

import com.kieling.simpsons.api.SimpcharApi;
import com.kieling.simpsons.screens.simpchars.SimpcharListActivity;
import com.kieling.simpsons.screens.simpchars.core.SimpcharsModel;
import com.kieling.simpsons.screens.simpchars.core.SimpcharsPresenter;
import com.kieling.simpsons.screens.simpchars.core.SimpcharsView;
import com.kieling.simpsons.utils.rx.RxSchedulers;

import dagger.Module;
import dagger.Provides;
import rx.subscriptions.CompositeSubscription;

@Module
public class SimpcharsModule {

    private SimpcharListActivity simpcharListContext;

    public SimpcharsModule(SimpcharListActivity context) {
        this.simpcharListContext = context;
    }

    @SimpcharsScope
    @Provides
    SimpcharsView provideView() {
        return new SimpcharsView(simpcharListContext);
    }

    @SimpcharsScope
    @Provides
    SimpcharsPresenter providePresenter(RxSchedulers schedulers, SimpcharsView view, SimpcharsModel model) {
        CompositeSubscription subscriptions = new CompositeSubscription();
        return new SimpcharsPresenter(schedulers, model, view, subscriptions);
    }

    @SimpcharsScope
    @Provides
    SimpcharListActivity provideContext() {
        return simpcharListContext;
    }

    @SimpcharsScope
    @Provides
    SimpcharsModel provideModel(SimpcharApi api) {
        return new SimpcharsModel(simpcharListContext, api);
    }
}