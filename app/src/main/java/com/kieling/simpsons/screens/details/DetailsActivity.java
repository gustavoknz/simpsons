package com.kieling.simpsons.screens.details;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kieling.simpsons.R;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
    }
}
