package com.kieling.simpsons.application.builder;

import com.kieling.simpsons.api.SimpcharApi;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
class SimpcharsApiServiceModule {

    private static final String BASE_URL = "https://j4g2c85pmf.execute-api.us-east-1.amazonaws.com/Prod/";

    @AppScope
    @Provides
    SimpcharApi provideApiService(OkHttpClient client, GsonConverterFactory gson, RxJavaCallAdapterFactory rxAdapter) {
        Retrofit retrofit = new Retrofit.Builder().client(client)
                .baseUrl(BASE_URL).addConverterFactory(gson)
                .addCallAdapterFactory(rxAdapter)
                .build();

        return retrofit.create(SimpcharApi.class);
    }
}
