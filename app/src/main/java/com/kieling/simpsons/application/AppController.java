package com.kieling.simpsons.application;

import android.app.Application;

import com.kieling.simpsons.application.builder.AppComponent;
import com.kieling.simpsons.application.builder.AppContextModule;
import com.kieling.simpsons.application.builder.DaggerAppComponent;

public class AppController extends Application {
    private static AppComponent appComponent;

    public static AppComponent getNetComponent() {
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initAppComponent();
    }

    private void initAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .appContextModule(new AppContextModule(this))
                .build();
    }
}
