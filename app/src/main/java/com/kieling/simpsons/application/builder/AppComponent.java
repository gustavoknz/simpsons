package com.kieling.simpsons.application.builder;

import com.kieling.simpsons.api.SimpcharApi;
import com.kieling.simpsons.utils.rx.RxSchedulers;

import dagger.Component;

@AppScope
@Component(modules = {NetworkModule.class, AppContextModule.class, RxModule.class, SimpcharsApiServiceModule.class})
public interface AppComponent {

    RxSchedulers rxSchedulers();

    SimpcharApi apiService();

}
