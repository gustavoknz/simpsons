package com.kieling.simpsons.application.builder;

import com.kieling.simpsons.utils.rx.AppRxSchedulers;
import com.kieling.simpsons.utils.rx.RxSchedulers;

import dagger.Module;
import dagger.Provides;

@Module
class RxModule {
    @Provides
    RxSchedulers provideRxSchedulers() {
        return new AppRxSchedulers();
    }
}
